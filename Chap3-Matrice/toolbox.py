import random


def createMat(size, t):
    m = []
    p = -1
    if t == "i":
        for i in range(size):
            p += 1
            m.append([])
            for j in range(size):
                if j == p:
                    m[-1].append(1)
                else:
                    m[-1].append(0)
    elif t == "r":
        for i in range(size):
            m.append([])
            for j in range(size):
                val = random.randint(0, 9)
                m[-1].append(val)
    elif type(t) == int or float:
        val = t
        for i in range(size):
            m.append([])
            for j in range(size):
                m[-1].append(val)
    else:
        print("error")

    return m


def copyMat(m):
    newM = []
    for ligne in m:
        newM.append(ligne[:])
    return newM


def printMat(m):
    for ligne in m:
        print(ligne)


def swapLine(m, l1, l2):
    cm = copyMat(m)
    tmp = cm[l1][:]
    cm[l1] = cm[l2][:]
    cm[l2] = tmp[:]


def sumLine(m, line):
    return sum(m[line])



def productMat(m1, m2):


def sumMat(m1, m2):
    len1 = len(m1)
    len2 = len(m1)
    if len1 != len2:
        return "Matrix have not the same size"
    else:
        m3 = createMat(len1, 0)
        for i in range(len1):
            for j in range(len1):
                m3[i][j] = m1[i][j] + m2[i][j]
        return m3

